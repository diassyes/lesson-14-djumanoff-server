package main

import (
	"encoding/json"
	"fmt"
	"github.com/djumanoff/amqp"
	"lesson-14-djumanoff-server/src/config"
	"lesson-14-djumanoff-server/src/domain"
	"lesson-14-djumanoff-server/src/rpc/rabbit"
)

func main() {
	fmt.Println("Server start")
	configs, err := config.GetConfigs()
	if err != nil {
		panic(err)
	}
	rabbitMQSession, err := rabbit.InitRabbitMQConnection(configs)
	if err != nil {
		panic(err)
	}
	defer (*rabbitMQSession).Close()

	rabbitMQServer, err := rabbit.GetRabbitMQServer(rabbitMQSession)
	if err != nil {
		panic(err)
	}

	_ = (*rabbitMQServer).Endpoint(
		"request.get.message",
		func(d amqp.Message) *amqp.Message {
			var clientMessage domain.Message
			err := json.Unmarshal(d.Body, &clientMessage)
			response := domain.Message{
				From:    "Server",
				To:      "Client",
				Text:    "Sorry, but somehow I don't care.",
				ReplyTo: clientMessage.Text,
			}
			responseBytesArr, err := json.Marshal(&response)
			if err != nil {
				panic(err)
			}
			return &amqp.Message{
				Body: responseBytesArr,
			}
		},
	)

	if err := (*rabbitMQServer).Start(); err != nil {
		panic(err)
	}

	fmt.Println("Server end")
}
