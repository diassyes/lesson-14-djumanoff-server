package domain

type Message struct {
	From string `json:"from"`
	To string `json:"to"`
	Text string `json:"text"`
	ReplyTo string `json:"reply_to,omitempty"`
}